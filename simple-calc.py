# Functions for calculator
def add(x, y):
    return x + y


def subtract(x, y):
    return x - y


def multiply(x, y):
    return x * y


def divide(x, y):
    return x / y


# MENU
print("""Select operation:
1. Add
2. Subtract
3. Multiply
4. Divide""")

opt = input("Enter option (1, 2, 3 or 4): ")
num1 = int(input("Enter first number: "))
num2 = int(input("Enter second number: "))

if opt == "1":
    print(num1, "+", num2, "=", add(num1, num2))
elif opt == "2":
    print(num1, "-", num2, "=", subtract(num1, num2))
elif opt == "3":
    print(num1, "*", num2, "=", multiply(num1, num2))
elif opt == "4":
    print(num1, "/", num2, "=", divide(num1, num2))
else:
    print("Invalid input")
